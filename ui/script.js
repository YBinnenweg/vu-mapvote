window.gameModeTranslations = {
	ConquestLarge0: 'Conquest Large',
	ConquestSmall0: 'Conquest',
	ConquestAssaultLarge0: 'Conquest Assault Large',
	ConquestAssaultSmall0: 'Conquest Assault',
	ConquestAssaultSmall1: 'Conquest Assault: Day 2',
	RushLarge0: 'Rush',
	SquadRush0: 'Squad Rush',
	SquadDeathMatch0: 'Squad Deathmatch',
	TeamDeathMatch0: 'Team Deathmatch',
	TeamDeathMatchC0: 'Team DM Close Quarters',
	Domination0: 'Conquest Domination',
	GunMaster0: 'Gun Master',
	TankSuperiority0: 'Tank Superiority',
	Scavenger0: 'Scavenger',
	CaptureTheFlag0: 'Capture the Flag',
	AirSuperiority0: 'Air Superiority'
};
window.mapTranslations = {
	MP_001: 'Grand Bazaar',
	MP_003: 'Teheran Highway',
	MP_007: 'Caspian Border',
	MP_011: 'Seine Crossing',
	MP_012: 'Operation Firestorm',
	MP_013: 'Damavand Peak',
	MP_017: 'Noshahr Canals',
	MP_018: 'Kharg Island',
	MP_Subway: 'Operation Metro',
	XP1_001: 'Strike at Karkand',
	XP1_002: 'Gulf of Oman',
	XP1_003: 'Sharqi Peninsula',
	XP1_004: 'Wake Island',
	XP2_Palace: 'Donya Fortress',
	XP2_Office: 'Operation 925',
	XP2_Factory: 'Scrapmetal',
	XP2_Skybar: 'Ziba Tower',
	XP3_Alborz: 'Alborz Mountains',
	XP3_Shield: 'Armored Shield',
	XP3_Desert: 'Bandar Desert',
	XP3_Valley: 'Death Valley',
	XP4_Parl: 'Azadi Palace',
	XP4_Quake: 'Epicenter',
	XP4_FD: 'Markaz Monolith',
	XP4_Rubble: 'Talah Market',
	XP5_001: 'Operation Riverside',
	XP5_002: 'Nebandan Flats',
	XP5_003: 'Kiasar Railroad',
	XP5_004: 'Sabalan Pipeline'
};
window.mapImages = {
	MP_001: 'UI/Art/Menu/LevelThumbs/MP01_thumb',
	MP_003: 'UI/Art/Menu/LevelThumbs/MP03_thumb',
	MP_007: 'UI/Art/Menu/LevelThumbs/MP07_thumb',
	MP_011: 'UI/Art/Menu/LevelThumbs/MP11_thumb',
	MP_012: 'UI/Art/Menu/LevelThumbs/MP12_thumb',
	MP_013: 'UI/Art/Menu/LevelThumbs/MP13_thumb',
	MP_017: 'UI/Art/Menu/LevelThumbs/MP17_thumb',
	MP_018: 'UI/Art/Menu/LevelThumbs/MP18_thumb',
	XP1_001: 'UI/Art/Menu/LevelThumbs/XP01_thumb',
	XP1_002: 'UI/Art/Menu/LevelThumbs/XP02_thumb',
	XP1_003: 'UI/Art/Menu/LevelThumbs/XP03_thumb',
	XP1_004: 'UI/Art/Menu/LevelThumbs/XP04_thumb',
	MP_Subway: 'UI/Art/Menu/LevelThumbs/MP15_thumb',
	XP2_Palace: 'UI/Art/Menu/LevelThumbs/XP2_Palace_thumb',
	XP2_Office: 'UI/Art/Menu/LevelThumbs/XP2_Office_thumb',
	XP2_Factory: 'UI/Art/Menu/LevelThumbs/XP2_Factory_thumb',
	XP2_Skybar: 'UI/Art/Menu/LevelThumbs/XP2_Skybar_thumb',
	XP3_Alborz: 'UI/Art/Menu/LevelThumbs/XP3_Alborz_thumb',
	XP3_Shield: 'UI/Art/Menu/LevelThumbs/XP3_Shield_thumb',
	XP3_Desert: 'UI/Art/Menu/LevelThumbs/XP3_Desert_thumb',
	XP3_Valley: 'UI/Art/Menu/LevelThumbs/XP3_Valley_thumb',
	XP4_Parl: 'UI/Art/Menu/LevelThumbs/XP4_Parl_thumb',
	XP4_Quake: 'UI/Art/Menu/LevelThumbs/XP4_Quake_thumb',
	XP4_FD: 'UI/Art/Menu/LevelThumbs/XP4_FD_thumb',
	XP4_Rubble: 'UI/Art/Menu/LevelThumbs/XP4_Rubble_thumb',
	XP5_001: 'UI/Art/Menu/LevelThumbs/XP5_001_thumb',
	XP5_002: 'UI/Art/Menu/LevelThumbs/XP5_002_thumb',
	XP5_003: 'UI/Art/Menu/LevelThumbs/XP5_003_thumb',
	XP5_004: 'UI/Art/Menu/LevelThumbs/XP5_004_thumb'
};
var votemapActive = false;

window.f1Pressed = () => {
	if (!votemapActive) return;
	if (getMenuStatus()) {
		setMenuStatus(false);
		WebUI.Call('ResetMouse');
		return;
	}
	setMenuStatus(true);
	WebUI.Call('EnableMouse');
};

function getMapTitle(key) {
	return window.mapTranslations[key]
}

function getGameModeTitle(key) {
	return window.gameModeTranslations[key]
}

function getMapImage(key) {
	if (key in window.mapImages) return 'fb://' + window.mapImages[key];
	return 'UI/Art/Menu/LevelThumbs/empty_thumb';
}

function getRandomMap() {
	return window.mapList[Math.floor(Math.random() * window.mapList.length)].id;
}

Number.prototype.toMMSS = function () {
	var sec_num = parseInt(this, 10); // don't forget the second param
	var hours = Math.floor(sec_num / 3600);
	var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	var seconds = sec_num - (hours * 3600) - (minutes * 60);

	if (minutes < 10) { minutes = "0" + minutes; }
	if (seconds < 10) { seconds = "0" + seconds; }
	return minutes + ':' + seconds;
}

window.startCountdown = (timer = 52 /* wait magical 52 seconds */) => {
	window.stopCountdown();
	window.timerHandle = setInterval(() => {
		timer--;
		document.getElementById("votemenu-counter").innerHTML = timer.toMMSS();
		if (timer <= 0) window.stopCountdown();
	}, 1000);
}
window.stopCountdown = () => {
	if (window.timerHandle !== null) clearInterval(window.timerHandle);
}

window.mapList = []; //init maplist
window.VotemapStart = (maps) => {
	window.selectedMap = null;
	setBannerStatus(true);
	window.voteStartAt = 0;
	votemapActive = true;
	window.VotemapStatus(maps);
	window.startCountdown();
}

window.getMapPage = () => {
	var maps = window.mapList.slice(window.voteStartAt).slice(0, 6).map((map) => ({
		id: map.id,
		name: map.name,
		title: getMapTitle(map.name),
		gameMode: getGameModeTitle(map.gameMode),
		rounds: map.rounds,
		image: getMapImage(map.name),
		votes: map.votes
	}));
	var missing = 0;
	if (maps.length < 6) missing = 6 - maps.length;
	for (var i = 0; i < missing; i++) {
		maps.push(null);
	}
	return maps;
};

window.voteStartAt = 0;
window.timer = null;
window.timerHandle = null;
window.selectedMap = null;

function voteItem(pageMapIndex) {
	let maps = window.getMapPage();
	vote(maps[pageMapIndex].id);
}

function vote(mapId) {
	window.selectedMap = mapId;
	window.renderSelections();
	setBannerStatus(false);
	setMenuStatus(false);
	WebUI.Call('ResetMouse');
	WebUI.Call('DispatchEvent', 'MapVote', mapId);
}

function randomVote() {
	vote(getRandomMap());
}

window.renderSelections = () => {
	window.getMapPage().forEach((map, index) => {
		var elem = document.getElementById("votemenu-vote" + (index + 1));
		elem.classList.remove('active');
		if (map == null) {
			elem.style['opacity'] = 0;
			return;
		}
		if (window.selectedMap == map.id) elem.classList.add('active');
		elem.style['opacity'] = 100;
		elem.style['background-image'] = `radial-gradient(transparent, black), url("${map.image}")`;
		let html = "";
		if (map.votes > 0)
			html += `<div class="voteitem-counter">${map.votes}</div>`
		html += `<div class="voteitem-footer">`;
		html += `<div class="voteitem-footer1">${map.gameMode}</div>`
		html += `<div class="voteitem-footer2">${map.title}</div>`
		html += '</div>';
		elem.innerHTML = html;
	});
}

function nextPage() {
	if (window.voteStartAt + 6 >= Object.keys(window.mapList).length) return;
	window.voteStartAt += 6;
	window.renderSelections();
}

function lastPage() {
	if (window.voteStartAt - 6 < 0) return;
	window.voteStartAt -= 6;
	window.renderSelections();
}

window.VotemapEnd = async (nextMapId) => {
	nextMapId = Number(nextMapId);

	window.stopCountdown();

	setBannerStatus(false);
	setMenuStatus(false);
	votemapActive = false;
	WebUI.Call('ResetMouse');

	const Toast = Swal.mixin({
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true
	})

	const nextMap = window.mapList.find(x => x.id == nextMapId);

	Toast.fire({
		icon: 'success',
		title: 'Next map: ' + window.mapTranslations[nextMap.name],
		text: window.gameModeTranslations[nextMap.gameMode]
	})
};

window.VotemapStatus = (maps) => {
	window.mapList = maps.filter(map => map.enabled); // only handle enabled maps, update maplist
	window.renderSelections();
};

function setMenuStatus(status = true) {
	document.getElementById("votemenu").style.display = status ? 'grid' : 'none';
}

function getMenuStatus() {
	return item = document.getElementById("votemenu").style.display == 'grid';
}

function setBannerStatus(status = true) {
	document.getElementById("keybanner").style.display = status ? 'block' : 'none';
}

function setListingStatus(status = true) {
	document.getElementById("votestatus").style.display = status ? 'block' : 'none';
}
