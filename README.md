# [Download this, do not just clone the git](https://gitlab.com/n4gi0s/vu-mapvote/-/jobs/artifacts/master/download?job=build)

### Contributors:
- @ensio

## Installation:

	Extract contect of zip to your mods folder. This should look something like Admin/Mods/vu-mapvote
	Add vu-mapvote to your Admin/ModList.txt
	Voting starts at the endscreen

## Optional configuration:

Add these to your Admin/Startup.txt

	mapvote.limit <number of selectable random maps>
	mapvote.excludecurrentmap <true/false>

## Manually starting a vote:

	mapvote.start
	mapvote.end

## In Game:

![](https://i.ibb.co/VDw63KV/1.png)

![](https://i.ibb.co/wwLzbdf/2.png)

![](https://i.ibb.co/c3yF0gM/5.png)
